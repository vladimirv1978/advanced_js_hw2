var books = [
  {
    author: "Люсі Фолі",
    name: "Список запрошених",
    price: 70,
  },
  {
    author: "Сюзанна Кларк",
    name: "Джонатан Стрейндж і м-р Норрелл",
  },
  {
    name: "Дизайн. Книга для недизайнерів.",
    price: 70,
  },
  {
    author: "Алан Мур",
    name: "Неономікон",
    price: 70,
  },
  {
    author: "Террі Пратчетт",
    name: "Рухомі картинки",
    price: 40,
  },
  {
    author: "Анґус Гайленд",
    name: "Коти в мистецтві",
  },
];

let listTag = document.querySelector("#root");
let ulTag = document.createElement("ul");
listTag.append(ulTag);
let arrSort = [];

function maxNumberElements(arr) {
  for (let elem of arr) {
    arrSort.push(Object.keys(elem).length);
  }
  let index = arrSort.findIndex((elem) => elem == Math.max(...arrSort));
  list(arr, index);
}

function list(arr, index) {
  for (let elem of arr) {
    let ulTagElement = document.createElement("ul");
    ulTagElement.className = "list-item";
    ulTag.append(ulTagElement);

    try {
      let missingItem = Object.keys(arr[index]).filter((nameKey) => {
        if (!elem.hasOwnProperty(nameKey)) return nameKey;
      });

      if (missingItem && missingItem != "") throw new SyntaxError(`${missingItem}`);

      for (let [key, value] of Object.entries(elem)) {
        let liTag = document.createElement("li");
        liTag.innerText = value;
        ulTagElement.append(liTag);
      }
    } catch (e) {
      console.log(`Отсутствует свойство "${e.message}" в объекте (в массиве "books" под индексом: "${arr.indexOf(elem)}")`);
      ulTagElement.remove();
    }
  }
}

maxNumberElements(books);
